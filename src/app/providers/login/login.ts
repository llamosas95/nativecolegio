
import { Injectable, Component, OnInit, ViewChild } from "@angular/core";

import { HttpClient, HttpHeaders } from "@angular/common/http";

// import { Http, Headers, Response } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class Login {
    // private serverUrl = "http://127.0.0.1:8000/api/login";
    private serverUrl ="http://cole.somossistemas.org/api/login";
    // http://127.0.0.1:8000/api/login
    constructor(private http: HttpClient) { }

    postData(data: any) {
        let options = this.createRequestOptions();
        return this.http.post(this.serverUrl,  data , { headers: options });
    }

    private createRequestOptions() {
        let headers = new HttpHeaders({
            "Content-Type": "application/json; charset=utf-8",
            "Accept": "application/json",
            'Access-Control-Allow-Origin': '*'
        });
        return headers;
    }


}