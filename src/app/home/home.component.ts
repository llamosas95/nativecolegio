import { Component, OnInit } from "@angular/core";
import {Login} from "../providers/login/login";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    providers: [Login]
})
export class HomeComponent implements OnInit {

    constructor(private loginProvider: Login) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    public submit() {
        this.makePostRequest();
    }

    private makePostRequest() {
        this.loginProvider
            .postData({ usuario: 'admin', clave: 'admin' })
            .subscribe(res => {
                const datos = (<any>res).message;
                console.log('--------->>>>>>>',datos);
            });
    }

}
